package com.moltean.data.persistence;

import android.test.AndroidTestCase;
import android.test.RenamingDelegatingContext;
import android.util.Log;

import com.moltean.exception.DataInsertException;
import com.moltean.model.Car;

import junit.framework.Assert;

import java.util.ArrayList;
import java.util.List;

/**
 * Test class for the database operations.
 */
public class CarsDatabaseHelperTest extends AndroidTestCase {

    private CarsDatabaseHelper carsDatabaseHelper;

    @Override
    public void setUp() throws Exception {
        super.setUp();
        RenamingDelegatingContext context = new RenamingDelegatingContext(getContext(), "test_");
        carsDatabaseHelper = new CarsDatabaseHelper(context);
    }

    public void testCreateDatabase() {
        assertNotNull(carsDatabaseHelper);
    }

    public void testWriteToDatabase() throws DataInsertException {
        List<Car> carList = initCarList();
        carsDatabaseHelper.writeCars(carList);
    }

    public void testWriteToDatabaseException() {
        List<Car> carList = initCarListWithError();
        try {
            carsDatabaseHelper.writeCars(carList);
            Assert.fail("Should have thrown DataInsertException when inserting a null value.");
        } catch (DataInsertException e) {
            Log.e(CarsDatabaseHelperTest.class.getSimpleName(), "Failure at inserting data into the database.", e);
        }
    }

    public void testGetCarsFromDatabase() throws DataInsertException {
        List<Car> cars = initCarList();
        carsDatabaseHelper.writeCars(cars);
        List<Car> carsFromDb = carsDatabaseHelper.getCars();
        assertTrue(carsFromDb.size() == 4);
        assertTrue("Car2".equals(carsFromDb.get(2).getTitle()));
        assertTrue(carsFromDb.get(0).getImages().size() == 1);
        assertTrue("Car0_0.jpg".equals(carsFromDb.get(0).getImages().get(0)));
    }

    private List<Car> initCarList() {
        List<Car> carList = new ArrayList<>();
        for (int i = 0; i < 4; i++) {
            Car car = new Car();
            car.setId(i);
            car.setTitle("Car" + i);
            car.setDate(System.currentTimeMillis() + (i * 100));
            car.setOwner("Owner" + i);
            car.setPdf("Car" + i + ".pdf");
            carList.add(car);
            List<String> imageFileNames = new ArrayList<>();
            for (int j = 0; j < i + 1; j++) {
                imageFileNames.add(car.getTitle() + "_" + j + ".jpg");
            }
            car.setImages(imageFileNames);
        }
        return carList;
    }

    private List<Car> initCarListWithError() {
        List<Car> carList = new ArrayList<>();
        Car car = new Car();
        car.setId(1);
        car.setTitle(null);
        car.setDate(System.currentTimeMillis());
        car.setOwner("Owner");
        car.setPdf("Car.pdf");
        carList.add(car);
        return carList;
    }

}
