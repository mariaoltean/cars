package com.moltean.exception;

/**
 * Exception thrown in case of an error while inserting data into the database.
 */
public class DataInsertException extends Exception {

    public DataInsertException(String message) {
        super(message);
    }
}
