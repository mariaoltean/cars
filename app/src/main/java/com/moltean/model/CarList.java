package com.moltean.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * List of {@link Car} objects.
 */
public class CarList {

    @SerializedName("cars")
    private final List<Car> cars = new ArrayList<>();

    public List<Car> getCars() {
        return cars;
    }
}
