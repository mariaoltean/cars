package com.moltean.model;

import com.google.gson.annotations.SerializedName;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Models a Car object.
 */
public class Car {

    private int id;
    @SerializedName("title")
    private String title;
    @SerializedName("date")
    private long date;
    @SerializedName("pdf")
    private String pdf;
    @SerializedName("owner")
    private String owner;
    @SerializedName("images")
    private List<String> images = new ArrayList<>();
    private String dateDisplayName;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public long getDate() {
        return date;
    }

    public String getPdf() {
        return pdf;
    }

    public String getOwner() {
        return owner;
    }

    public List<String> getImages() {
        return images;
    }


    public void setDate(long date) {
        this.date = date;
    }

    public void setPdf(String pdf) {
        this.pdf = pdf;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setDateDisplayName(long date) {
        SimpleDateFormat df = new SimpleDateFormat("d MMM yyyy hh:mm", Locale.getDefault());
        this.dateDisplayName = df.format(date);
    }

    public String getDateDisplayName() {
        return dateDisplayName;
    }

    @Override
    public String toString() {
        return "Car: [" + title + ", " + date + ", " + owner + "]";
    }
}