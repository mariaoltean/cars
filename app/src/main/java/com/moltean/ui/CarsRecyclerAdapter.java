package com.moltean.ui;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.moltean.cars.R;
import com.moltean.model.Car;
import com.moltean.util.AnimationUtil;
import com.moltean.util.PDFOpener;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.ArrayList;
import java.util.List;

/**
 * Adapter that provides the binding between the {@see Car} related data and the views displayed within a RecyclerView.
 */
public class CarsRecyclerAdapter extends RecyclerView.Adapter<CarsRecyclerAdapter.CarViewHolder> {


    private final List<Car> cars = new ArrayList<>();
    private final Handler handler;
    private final Context context;
    private final int imageWidth;

    public CarsRecyclerAdapter(Context context, Handler handler) {
        this.context = context;
        this.handler = handler;
        imageWidth = context.getResources().getDimensionPixelSize(R.dimen.car_image_width);
    }

    @Override
    public CarViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.car_tile, viewGroup, false);
        return new CarViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final CarViewHolder carViewHolder, int position) {
        carViewHolder.bindView(position);
    }

    @Override
    public int getItemCount() {
        return cars.size();
    }


    class CarViewHolder extends RecyclerView.ViewHolder {
        ImageLoaderRunnable imageLoaderRunnable;
        final TextView titleHolder;
        final TextView dateHolder;
        final TextView ownerHolder;
        final ImageView imageHolder;
        int imageIndex;
        int id;

        public CarViewHolder(View view) {
            super(view);
            titleHolder = (TextView) view.findViewById(R.id.titleHolder);
            dateHolder = (TextView) view.findViewById(R.id.dateHolder);
            ownerHolder = (TextView) view.findViewById(R.id.ownerHolder);
            imageHolder = (ImageView) view.findViewById(R.id.imageHolder);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    openPdf();
                }

                private void openPdf() {
                    final Car car = cars.get(getAdapterPosition());
                    new AsyncTask<Void, Void, Boolean>() {

                        @Override
                        protected void onPreExecute() {
                            Toast.makeText(context, "Opening " + car.getPdf(), Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        protected Boolean doInBackground(Void... params) {
                            return PDFOpener.openPdf(car.getPdf());
                        }

                        @Override
                        protected void onPostExecute(Boolean success) {
                            if (!success) {
                                Toast.makeText(context, car.getPdf() + " could not be opened. Please check if you have a PDF viewer application installed.", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }.execute();
                }
            });

        }

        private void bindView(int position) {
            Car car = cars.get(position);
            id = car.getId();
            titleHolder.setText(car.getTitle());
            ownerHolder.setText(car.getOwner());
            dateHolder.setText(car.getDateDisplayName());
            if (!car.getImages().isEmpty()) {
                imageLoaderRunnable = new ImageLoaderRunnable(car, this, position);
                handler.post(imageLoaderRunnable);
            }
        }
    }

    public void setData(List<Car> cars) {
        this.cars.clear();
        this.cars.addAll(cars);
        notifyDataSetChanged();
    }

    private class ImageLoaderRunnable implements Runnable {

        private static final String ANDROID_ASSETS_PATH = "file:///android_asset/";
        public static final int IMAGE_LOADING_DELAY_MILLIS = 3000;
        private final Car car;
        private final int position;
        private final List<String> carImages;
        private final CarViewHolder carViewHolder;

        ImageLoaderRunnable(Car car, CarViewHolder carViewHolder, int position) {
            this.car = car;
            this.position = position;
            this.carImages = car.getImages();
            this.carViewHolder = carViewHolder;
        }

        @Override
        public void run() {
            if (carImages.size() > 1) {
                // start over with the first image
                if (carViewHolder.imageIndex == carImages.size()) {
                    carViewHolder.imageIndex = 0;
                }
                if (carViewHolder.imageIndex < carImages.size()) {
                    loadImage(carViewHolder, carImages.get(carViewHolder.imageIndex), position);
                }
                handler.postDelayed(new ImageLoaderRunnable(car, carViewHolder, position), IMAGE_LOADING_DELAY_MILLIS);
                carViewHolder.imageIndex++;
            } else {
                loadImage(carViewHolder, carImages.get(carViewHolder.imageIndex), position);
            }
        }

        private void loadImage(CarViewHolder carViewHolder, String imageUrl, int position) {
            Target imageTarget = new ImageTarget(carViewHolder.imageHolder);
            if (cars.get(position).getId() == carViewHolder.id) {
                Picasso.with(context)
                        .load(ANDROID_ASSETS_PATH + imageUrl)
                        .resize(imageWidth, 0)
                        .placeholder(R.drawable.ic_launcher)
                        .into(imageTarget);
            }
        }
    }

    private class ImageTarget implements Target {

        private final ImageView imageHolder;

        public ImageTarget(ImageView imageHolder) {
            this.imageHolder = imageHolder;
        }

        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
            if (imageHolder.isShown()) {
                AnimationUtil.animateImageViewBitmap(imageHolder, bitmap);
            }
        }

        @Override
        public void onBitmapFailed(Drawable errorDrawable) {
            imageHolder.setImageResource(R.drawable.ic_launcher);
        }

        @Override
        public void onPrepareLoad(Drawable placeHolderDrawable) {
            // do nothing
        }
    }

}

