package com.moltean.ui;

import android.app.Activity;
import android.app.LoaderManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.Loader;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.moltean.cars.R;
import com.moltean.data.loader.CarsAsyncTaskLoader;
import com.moltean.model.Car;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents the application's main screen which displays the list of cars.
 */
public class MainActivity extends Activity implements LoaderManager.LoaderCallbacks<List<Car>> {

    private final DataParsingStatusReceiver receiver = new DataParsingStatusReceiver();

    private static final int LOADER_ID = 0;
    private CarsRecyclerAdapter carsRecyclerAdapter;
    private final Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.carListView);
        RecyclerView.ItemDecoration itemDecoration = new DividerItemDecoration(this);
        recyclerView.addItemDecoration(itemDecoration);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        carsRecyclerAdapter = new CarsRecyclerAdapter(this, handler);
        recyclerView.setAdapter(carsRecyclerAdapter);
        getLoaderManager().initLoader(LOADER_ID, null, this);
    }

    @Override
    public Loader<List<Car>> onCreateLoader(int id, Bundle args) {
        return new CarsAsyncTaskLoader(this);
    }

    @Override
    public void onLoadFinished(Loader<List<Car>> loader, List<Car> data) {
        carsRecyclerAdapter.setData(data);
    }

    @Override
    public void onLoaderReset(Loader<List<Car>> loader) {
        carsRecyclerAdapter.setData(new ArrayList<Car>());
    }


    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, new IntentFilter(DataParsingStatusReceiver.ACTION));
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
        if (isFinishing()) {
            handler.removeCallbacksAndMessages(null);
        }
    }

    /**
     * Receiver notified of the status of data parsing.
     */
    public static class DataParsingStatusReceiver extends BroadcastReceiver {

        public static final String ACTION = "com.moltean.cars.JSON_PARSING_STATUS";
        public static final String EXTRA_MESSAGE = "com.moltean.cars.EXTRA_MESSAGE";


        @Override
        public void onReceive(Context context, Intent intent) {
            if (ACTION.equals(intent.getAction())) {
                String message = intent.getStringExtra(EXTRA_MESSAGE);
                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();

            }
        }
    }

}
