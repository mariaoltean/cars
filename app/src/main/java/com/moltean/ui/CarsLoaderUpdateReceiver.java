package com.moltean.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.Loader;

/**
 * Broadcast receiver used for observing the changes in loading the data; it notifies the loader when the data is written
 * in the database in order for the UI to get updated.
 */
public class CarsLoaderUpdateReceiver extends BroadcastReceiver {

    private final Loader loader;

    public static final String ACTION = "com.moltean.cars.CONTENT_CHANGED_ACTION";

    public CarsLoaderUpdateReceiver(Loader loader) {
        this.loader = loader;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (ACTION.equals(intent.getAction())) {
            loader.onContentChanged();

        }
    }
}
