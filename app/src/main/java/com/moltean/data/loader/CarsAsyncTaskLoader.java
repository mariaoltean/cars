package com.moltean.data.loader;

import android.content.AsyncTaskLoader;
import android.content.Context;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;

import com.moltean.data.persistence.CarsDatabaseHelper;
import com.moltean.model.Car;
import com.moltean.ui.CarsLoaderUpdateReceiver;

import java.util.List;

/**
 * Loads the list of {@see Car} items from the database asynchronously.
 */
public class CarsAsyncTaskLoader extends AsyncTaskLoader<List<Car>> {

    private final Context context;

    private List<Car> mData;
    private CarsLoaderUpdateReceiver updateReceiver;

    public CarsAsyncTaskLoader(Context context) {
        super(context);
        this.context = context;
    }

    @Override
    public List<Car> loadInBackground() {
        return new CarsDatabaseHelper(context).getCars();
    }


    @Override
    public void deliverResult(List<Car> data) {
        if (isReset()) {
            releaseResources(data);
            return;
        }
        List<Car> oldData = mData;
        mData = data;
        if (isStarted()) {
            super.deliverResult(data);
        }
        if (oldData != null && oldData != data) {
            releaseResources(oldData);
        }
    }

    @Override
    protected void onStartLoading() {
        if (mData != null) {
            deliverResult(mData);
        }
        if (updateReceiver == null) {
            updateReceiver = new CarsLoaderUpdateReceiver(this);
            LocalBroadcastManager.getInstance(context).registerReceiver(updateReceiver, new IntentFilter(CarsLoaderUpdateReceiver.ACTION));
        }

        if (takeContentChanged() || mData == null) {
            forceLoad();
        }
    }

    @Override
    protected void onStopLoading() {
        cancelLoad();
    }

    @Override
    protected void onReset() {
        onStopLoading();
        if (mData != null) {
            releaseResources(mData);
            mData = null;
        }
        if (updateReceiver != null) {
            LocalBroadcastManager.getInstance(context).unregisterReceiver(updateReceiver);
            updateReceiver = null;
        }
    }

    @Override
    public void onCanceled(List<Car> data) {
        super.onCanceled(data);
        releaseResources(data);
    }

    private void releaseResources(List<Car> data) {
        // do nothing
    }

}
