package com.moltean.data.persistence;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.moltean.exception.DataInsertException;
import com.moltean.model.Car;

import java.util.ArrayList;
import java.util.List;

/**
 * Manages the database setup and the operations against its data.
 */
public class CarsDatabaseHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "cars.db";
    private static final int DATABASE_VERSION = 1;

    private static final String TABLE_CARS = "car";
    private static final String TABLE_CAR_IMAGES = "car_image";

    private static final String COLUMN_ID = "_id";
    private static final String COLUMN_TITLE = "title";
    private static final String COLUMN_DATE = "date";
    private static final String COLUMN_PDF_NAME = "pdf";
    private static final String COLUMN_OWNER = "owner";

    private static final String COLUMN_CAR_ID = "_id";
    private static final String COLUMN_IMAGE_FILE_NAME = "image_name";

    private static final String CREATE_TABLE_CARS = "CREATE TABLE "
            + TABLE_CARS + "( "
            + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + COLUMN_TITLE + " TEXT NOT NULL, "
            + COLUMN_DATE + " LONG DEFAULT 0, "
            + COLUMN_PDF_NAME + " TEXT NOT NULL, "
            + COLUMN_OWNER + " TEXT NOT NULL"
            + ");";

    private static final String CREATE_TABLE_CAR_IMAGES = "CREATE TABLE "
            + TABLE_CAR_IMAGES + "( "
            + COLUMN_CAR_ID + " INTEGER DEFAULT 0, "
            + COLUMN_IMAGE_FILE_NAME + " TEXT NOT NULL, "
            + "FOREIGN KEY (" + COLUMN_CAR_ID + ") REFERENCES " + TABLE_CARS + "(" + COLUMN_ID + "));";

    private final SQLiteDatabase db;

    public CarsDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.db = getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_CARS);
        db.execSQL(CREATE_TABLE_CAR_IMAGES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CARS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CAR_IMAGES);
        onCreate(db);
    }

    /**
     * Writes a list of {{@link Car} items to the database.
     *
     * @param cars the list to be persisted.
     * @throws DataInsertException thrown in case of an error while inserting data.
     */
    public void writeCars(List<Car> cars) throws DataInsertException {
        for (Car car : cars) {
            ContentValues values = new ContentValues();
            values.put(CarsDatabaseHelper.COLUMN_TITLE, car.getTitle());
            values.put(CarsDatabaseHelper.COLUMN_DATE, car.getDate());
            values.put(CarsDatabaseHelper.COLUMN_OWNER, car.getOwner());
            values.put(CarsDatabaseHelper.COLUMN_PDF_NAME, car.getPdf());

            long carRowId = db.insert(CarsDatabaseHelper.TABLE_CARS, null, values);
            if (carRowId == -1) {
                throw new DataInsertException("Data could not be inserted into the " + TABLE_CARS + " table.");
            }
            for (String imageName : car.getImages()) {
                ContentValues imageValues = new ContentValues();
                imageValues.put(CarsDatabaseHelper.COLUMN_CAR_ID, carRowId);
                imageValues.put(CarsDatabaseHelper.COLUMN_IMAGE_FILE_NAME, imageName);
                long carImageRowId = db.insert(CarsDatabaseHelper.TABLE_CAR_IMAGES, null, imageValues);
                if (carImageRowId == -1) {
                    throw new DataInsertException("Data could not be inserted into the " + TABLE_CARS + " table.");
                }
            }
        }
    }

    /**
     * Queries the database in order to retrieve the persisted cars.
     *
     * @return a list containing all the cars in the database.
     */
    public List<Car> getCars() {
        List<Car> cars = new ArrayList<>();
        Cursor cursor = null;
        try {
            cursor = db.query(CarsDatabaseHelper.TABLE_CARS, null, null, null, null, null, null);
            Cursor imagesCursor = null;
            cursor.moveToFirst();
            try {
                while (!cursor.isAfterLast()) {
                    Car car = buildCar(cursor);
                    cars.add(car);
                    imagesCursor = db.query(CarsDatabaseHelper.TABLE_CAR_IMAGES, null, CarsDatabaseHelper.COLUMN_CAR_ID + "=?", new String[]{String.valueOf(car.getId())}, null, null, null);
                    imagesCursor.moveToFirst();
                    List<String> carImages = new ArrayList<>();
                    while (!imagesCursor.isAfterLast()) {
                        String carImage = imagesCursor.getString(imagesCursor.getColumnIndex(CarsDatabaseHelper.COLUMN_IMAGE_FILE_NAME));
                        carImages.add(carImage);
                        imagesCursor.moveToNext();
                    }

                    car.setImages(carImages);
                    cursor.moveToNext();
                }
            } finally {
                if (imagesCursor != null) {
                    imagesCursor.close();
                }
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }

        return cars;
    }

    private Car buildCar(Cursor cursor) {
        Car car = new Car();
        car.setId(cursor.getInt(cursor.getColumnIndex(CarsDatabaseHelper.COLUMN_ID)));
        car.setTitle(cursor.getString(cursor.getColumnIndex(CarsDatabaseHelper.COLUMN_TITLE)));
        long date = cursor.getLong(cursor.getColumnIndex(CarsDatabaseHelper.COLUMN_DATE));
        car.setDate(date);
        car.setDateDisplayName(date);
        car.setOwner(cursor.getString(cursor.getColumnIndex(CarsDatabaseHelper.COLUMN_OWNER)));
        car.setPdf(cursor.getString(cursor.getColumnIndex(CarsDatabaseHelper.COLUMN_PDF_NAME)));
        return car;
    }
}
