package com.moltean.data.parser;

import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import com.moltean.cars.R;
import com.moltean.model.Car;
import com.moltean.model.CarList;
import com.moltean.ui.MainActivity;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

/**
 * Parses a JSON file defined in the application's "assets" folder.
 */
public class CarsJsonParser {

    public static final String JSON_FILE = "car_list.json";
    private static final String TAG = CarsJsonParser.class.getSimpleName();

    /**
     * Parses the json data given in the specific file.
     *
     * @param context  application context.
     * @param fileName the name of the JSON file to be parsed.
     * @return the list of cars retrieved from the file.
     */
    public static List<Car> parseCars(Context context, String fileName) {

        AssetManager assetManager = context.getAssets();
        InputStream inputStream;
        try {
            inputStream = assetManager.open(fileName);
        } catch (IOException e) {
            Log.e(TAG, "The JSON file could not be opened.", e);
            return new ArrayList<>();
        }
        Gson gson = new Gson();
        Reader reader = new InputStreamReader(inputStream);

        try {
            return gson.fromJson(reader, CarList.class).getCars();
        } catch (JsonParseException e) {
            Log.e(CarsJsonParser.class.getSimpleName(), "An error occurred while parsing data from " + fileName + " in assets.", e);
            sendParsingNotification(context, context.getString(R.string.data_parsing_error, fileName));
            return new ArrayList<>();
        }

    }

    private static void sendParsingNotification(Context context, String message) {
        Intent intent = new Intent(MainActivity.DataParsingStatusReceiver.ACTION);
        intent.putExtra(MainActivity.DataParsingStatusReceiver.EXTRA_MESSAGE, message);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

}
