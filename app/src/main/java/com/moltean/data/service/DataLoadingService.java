package com.moltean.data.service;

import android.app.IntentService;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.moltean.data.parser.CarsJsonParser;
import com.moltean.data.persistence.CarsDatabaseHelper;
import com.moltean.exception.DataInsertException;
import com.moltean.model.Car;
import com.moltean.ui.CarsLoaderUpdateReceiver;
import com.moltean.util.SharedPrefsHelper;

import java.util.List;

/**
 * Service managing the initial data loading in the background.
 */
public class DataLoadingService extends IntentService {

    public DataLoadingService() {
        super(DataLoadingService.class.getSimpleName());
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        List<Car> cars = CarsJsonParser.parseCars(getApplicationContext(), CarsJsonParser.JSON_FILE);
        try {
            if (cars.isEmpty()) return;
            new CarsDatabaseHelper(getApplicationContext()).writeCars(cars);
            SharedPrefsHelper.setDataLoaded();
            LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(new Intent(CarsLoaderUpdateReceiver.ACTION));
        } catch (DataInsertException e) {
            Log.e(DataLoadingService.class.getSimpleName(), "An error occurred while inserting data into the database.", e);
        }
    }
}
