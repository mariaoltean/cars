package com.moltean;

import android.app.Application;
import android.content.Context;
import android.content.Intent;

import com.moltean.data.service.DataLoadingService;
import com.moltean.util.SharedPrefsHelper;

/**
 * The application's main entry point.
 */
public class CarsApplication extends Application {

    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        CarsApplication.context = getApplicationContext();
        if (!SharedPrefsHelper.isDataLoaded()) {
            Intent intent = new Intent(this, DataLoadingService.class);
            startService(intent);
        }
    }

    /**
     * Retrieves the app's context.
     *
     * @return the application context.
     */
    public static Context getContext() {
        return CarsApplication.context;
    }
}
