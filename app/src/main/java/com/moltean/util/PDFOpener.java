package com.moltean.util;

import android.content.Intent;
import android.content.res.AssetManager;
import android.net.Uri;
import android.util.Log;

import com.moltean.CarsApplication;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Utility class managing the opening of PDF files.
 */
public class PDFOpener {

    private static final String TAG = PDFOpener.class.getSimpleName();

    /**
     * Opens a PDF file.
     *
     * @param path the path of the file.
     */
    public static boolean openPdf(String path) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        File file = new File(CarsApplication.getContext().getExternalFilesDir(null), path);
        if (!file.exists()) {
            if (!copyFileFromAssetsToExternalStorage(path)) {
                return false;
            }
        }
        Uri uri = Uri.fromFile(file);

        intent.setDataAndType(uri, "application/pdf");

        // check if there is a app that can open PDFs
        if (intent.resolveActivity(CarsApplication.getContext().getPackageManager()) == null) {
            return false;
        }
        intent.setData(uri);
        CarsApplication.getContext().startActivity(intent);
        NotificationUtil.createNotification(path);
        return true;
    }

    private static boolean copyFileFromAssetsToExternalStorage(String fileName) {
        AssetManager assetManager = CarsApplication.getContext().getAssets();
        InputStream in = null;
        OutputStream out = null;
        try {
            in = assetManager.open(fileName);
            File outFile = new File(CarsApplication.getContext().getExternalFilesDir(null), fileName);
            out = new FileOutputStream(outFile);
            copyFile(in, out);
        } catch (IOException e) {
            Log.e(TAG, "Failure at copying asset file: " + fileName, e);
            return false;
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    Log.e(TAG, "Failed to close the input stream.", e);
                }
            }
            if (out != null) {
                try {
                    out.close();
                } catch (IOException e) {
                    Log.e(TAG, "Failed to close the output stream.", e);
                }
            }
        }
        return true;
    }

    private static void copyFile(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[1024];
        int read;
        while ((read = in.read(buffer)) != -1) {
            out.write(buffer, 0, read);
        }
    }

}
