package com.moltean.util;

import android.graphics.Bitmap;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;

/**
 * Utility class managing animations.
 */
public class AnimationUtil {

    private static final long ANIMATION_DURATION = 1200;

    /**
     * Animates the bitmap while placing it into an image view.
     *
     * @param imageView the image view holding the bitmap.
     * @param bitmap    the bitmap to be set for the image view.
     */
    public static void animateImageViewBitmap(final ImageView imageView, final Bitmap bitmap) {
        final AlphaAnimation fadeIn = new AlphaAnimation(0, 1);
        fadeIn.setInterpolator(new DecelerateInterpolator());
        fadeIn.setDuration(ANIMATION_DURATION);

        final AlphaAnimation fadeOut = new AlphaAnimation(1, 0);
        fadeOut.setInterpolator(new AccelerateInterpolator());
        fadeOut.setDuration(ANIMATION_DURATION);

        fadeOut.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                // do nothing
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                // do nothing
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                imageView.setImageBitmap(bitmap);
                imageView.startAnimation(fadeIn);
            }
        });
        imageView.startAnimation(fadeOut);
    }
}
