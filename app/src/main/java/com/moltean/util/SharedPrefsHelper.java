package com.moltean.util;

import android.content.SharedPreferences;

import com.moltean.CarsApplication;

/**
 * Helper class for managing the data persisted into the shared preferences.
 */
public class SharedPrefsHelper {

    private static final String PREFS_NAME = "DATA_LOADED_PREFS_NAME";
    private static final String DATA_LOADED_KEY = "DATA_LOADED_KEY";

    /**
     * Saves a flag into the shared prefs file representing the successful loading of the data at the application's startup.
     */
    public static void setDataLoaded() {
        SharedPreferences sharedPreferences = CarsApplication.getContext().getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(DATA_LOADED_KEY, true).apply();
    }

    /**
     * Checks the flag representing the status of the data loading.
     *
     * @return true if the data was successfully loaded at the application's startup or false otherwise.
     */
    public static boolean isDataLoaded() {
        SharedPreferences sharedPreferences = CarsApplication.getContext().getSharedPreferences(PREFS_NAME, 0);
        return sharedPreferences.getBoolean(DATA_LOADED_KEY, false);
    }
}
