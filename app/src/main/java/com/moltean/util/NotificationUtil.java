package com.moltean.util;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

import com.moltean.CarsApplication;
import com.moltean.cars.R;
import com.moltean.ui.MainActivity;

/**
 * Utility class for managing notifications({@link android.support.v4.app.NotificationCompat}).
 */
class NotificationUtil {

    private static final int NOTIFICATION_ID = 10;

    /**
     * Create a status bar notification when opening a PDF file. When clicking the notification the application's main activity is being
     * brought to the front and the notification is dismissed.
     *
     * @param fileName the name of the file being opened.
     */
    public static void createNotification(String fileName) {

        Intent showIntent = new Intent(CarsApplication.getContext(), MainActivity.class);
        PendingIntent contentIntent = PendingIntent.getActivity(CarsApplication.getContext(), 0, showIntent, 0);

        NotificationManager notificationManager = (NotificationManager) CarsApplication.getContext().getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(CarsApplication.getContext())
                .setSmallIcon(R.drawable.ic_launcher)
                .setContentTitle("Download Notification")
                .setContentText(fileName)
                .setContentIntent(contentIntent)
                .setAutoCancel(true);

        notificationManager.notify(NOTIFICATION_ID, builder.build());
    }
}